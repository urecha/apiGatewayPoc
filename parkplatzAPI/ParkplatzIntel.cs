namespace parkplatzAPI
{
    public class ParkplatzIntel
    {
        public int Free { get; set; }

        public string Status { get; set; } = "";

        public IEnumerable<Price> Prices { get; set; } = new List<Price>();
    }

    public class Price
    {
        public string Hours { get; set; } = "";
        public float Cost { get; set; }
    }
}