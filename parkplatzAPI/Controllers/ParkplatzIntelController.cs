using Microsoft.AspNetCore.Mvc;
using System.Text.Json;

namespace parkplatzAPI.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ParkplatzIntelController : ControllerBase
    {
        private readonly ILogger<ParkplatzIntelController> _logger;

        public ParkplatzIntelController(ILogger<ParkplatzIntelController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public async Task<ParkplatzIntel> GetAsync()
        {
            ParkplatzIntel intel = new ParkplatzIntel
            {
                Free = 61,
                Status = "Geschlossen",
                Prices = new List<Price> {
                    new Price { Hours = "1h", Cost = 1 },
                    new Price { Hours = "2h", Cost = 3 },
                    new Price { Hours = "3h", Cost = 5 },
                    new Price { Hours = "4h", Cost = 8 },
                    new Price { Hours = "Jede weitere h", Cost = 3 },
                    new Price { Hours = "Max. / Tag", Cost = 34 },
                }
            };

            return intel;
        }
    }
}