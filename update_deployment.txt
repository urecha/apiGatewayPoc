# Deployment script 
# Simply restarts the docker compose, pulling the latest images.
# Must be placed in the same folder as the docker-compose.yml on the host system

# Stop current composition
docker-compose down --rmi all

# Start the composition (with latest images)
docker-compose up
