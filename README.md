# M326 API Gateway POC Project

## Hosting

The three services (API Gateway, Parkplatz API & Weatherforecast API) are configured to run in Docker containers. We're using `docker-compose` to then bundle them as a single unit.

To build the images, run the following command in the root folder

```bat
docker-compose build
```

Then run this to fire up the containers
```bat
docker-compose up
```

Navigate to *localhost:5902/api/v1/weather/weatherforecast* to get some intel on the upcoming weather then, or check out how many free spaces there are in the parking garage on *localhost:5902/api/v1/parkplatz/parkplatzintel*
